import { StyleSheet, PixelRatio } from "react-native";
import { colors, metrics } from "styles";

const styles = StyleSheet.create({
  icon: {
    color: colors.primary
  },

  row: {
    flexDirection: "column"
  },

  rowSpecialAttention: {
    borderLeftWidth: metrics.smallSpace + 2,
    borderLeftColor: colors.danger,
    paddingLeft: metrics.smallSpace
  },

  leftBarDescription: {
    backgroundColor: colors.regular,
    width: metrics.smallSpace,
    marginRight: metrics.smallSpace
  },

  title: {
    fontSize: 14,
    fontWeight: "bold"
  },

  subTitle: {
    fontSize: 12
  },

  user: {
    marginTop: metrics.baseMargin,
    padding: metrics.smallSpace,
    fontSize: metrics.subTitle
  },

  detail: {
    backgroundColor: colors.white,
    margin: metrics.smallSpace,
    fontSize: metrics.subTitle
  },

  jobContainer: {
    marginBottom: metrics.basePadding,
    borderWidth: 1,
    borderRadius: metrics.borderRadius,
    borderColor: colors.dark,
    backgroundColor: colors.white
  },

  jobContainerWithPadding: {
    paddingTop: metrics.basePadding
  },

  address: {
    paddingLeft: metrics.baseMargin
  },

  addressContainer: {
    backgroundColor: colors.lighter
  },

  button: {
    alignSelf: "stretch",
    height: 38,
    justifyContent: "center",
    alignItems: "center",
    marginTop: metrics.baseMargin
  },
  buttonText: {
    fontWeight: "bold",
    color: colors.white,
    fontSize: 14
  },

  avatarContainer: {
    borderColor: colors.dark,
    borderWidth: 2 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center"
  },

  buttonDontRealize: {
    backgroundColor: colors.danger
  },

  blockTitle: {
    alignSelf: "center",
    margin: 10,
    marginTop: -5
  },
  avatar: {
    // borderRadius: 75,
    width: 135,
    height: 135,
    margin: metrics.baseMargin
  },

  signatureText: {
    fontSize: 24,
    fontWeight: "bold",
    alignItems: "center",
    justifyContent: "center"
  },
  calendarContainer: {
    alignSelf: "center",
    marginBottom: metrics.basePadding
  }
});

export default styles;
