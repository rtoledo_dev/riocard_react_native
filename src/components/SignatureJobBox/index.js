import React, { Component } from 'react';
import { View, Text, TouchableHighlight, AsyncStorage } from "react-native";
import { withNavigation } from "react-navigation";
import SignatureCapture from 'react-native-signature-capture';
import api from "services/api";

import styles from './styles';

class SignatureJobBox extends Component {
  state = {
    loading: false
  };

  detailJob = async (job) => {
    this.setState({ loading: true });
    try {
      const auth_token = await AsyncStorage.getItem("@ClientKey:auth_token");
      await this.saveJob(job, auth_token); //aqui vai o ajax
      this.setState({ loading: false });
      this.props.navigation.navigate("DetailJob", { job });
    } catch (err) {
      console.tron.log(err);
      this.setState({
        loading: false,
        errorMessage: "Falha na operacao"
      });
    }
  };

  saveJob = async (job, auth_token) => {
    const resultCheckin = await api.post('/jobs/checkin', {
      auth_token: auth_token,
      id: job.id,
      loading: false,
      errorMessage: null
    });
  };

  saveSign() {
      this.refs["sign"].saveImage();
  }

  resetSign() {
      this.refs["sign"].resetImage();
  }

  _onSaveEvent(result) {
      //result.encoded - for the base64 encoded png
      //result.pathName - for the file path name
      console.log(result);
  }
  _onDragEvent() {
        // This callback will be called when the user enters signature
      console.log("dragged");
  }

  render() {
    return (
      <View style={{ flex: 1, flexDirection: "column", alignItems:"center",justifyContent:"center" }}>
                <Text style={{alignItems:"center",justifyContent:"center"}}>Signature Capture Extended </Text>
                <SignatureCapture
                    style={[{flex:1},styles.signature]}
                    ref="sign"
                    onSaveEvent={this._onSaveEvent}
                    onDragEvent={this._onDragEvent}
                    saveImageFileInExtStorage={false}
                    showNativeButtons={false}
                    showTitleLabel={false}
                    viewMode={"portrait"}/>

                <View style={{ flex: 1, flexDirection: "row" }}>
                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.saveSign() } } >
                        <Text>Salvar assinatura</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.resetSign() } } >
                        <Text>Apagar</Text>
                    </TouchableHighlight>

                </View>

            </View>
    );
  }
};

export default withNavigation(SignatureJobBox);
