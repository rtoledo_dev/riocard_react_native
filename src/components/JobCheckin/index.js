import React, { Component } from "react";
import { View, Text, TouchableOpacity, ActivityIndicator } from "react-native";
import { withNavigation } from "react-navigation";

import styles from "./styles";

class JobCheckin extends Component {
  state = {
    loading: false
  };

  render() {
    return (
      <View style={styles.jobContainer}>
        <View style={styles.row}>
          <View style={styles.rowSpecialAttention}>
            <Text style={styles.title}>OS #{this.props.job.id}</Text>
            <Text style={styles.title}>
              Situação: {this.props.job.status_name}
            </Text>
            <Text style={styles.subTitle}>Descrição: {this.props.job.description}</Text>
            <Text style={styles.subTitle}>Observação: {this.props.job.emitter_comments}</Text>
          </View>
          <View>
            <Text style={styles.detail}>
              {JSON.parse(this.props.job.request_document).endereco},{" "}
              {JSON.parse(this.props.job.request_document).bairro} -{" "}
              {JSON.parse(this.props.job.request_document).municipio} /{" "}
              {JSON.parse(this.props.job.request_document).estado}
            </Text>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.realizeJob(this.props.job)}
            >
              {this.state.loading ? (
                <ActivityIndicator size="small" color="#fff" />
              ) : (
                <Text style={styles.buttonText}>Realizar Check in</Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default withNavigation(JobCheckin);
