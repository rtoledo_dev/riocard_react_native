import { StyleSheet, PixelRatio } from "react-native";
import { colors, metrics } from "styles";

const styles = StyleSheet.create({
  icon: {
    color: colors.primary
  },

  row: {
    flexDirection: "column"
  },

  rowSpecialAttention: {
    borderLeftWidth: metrics.smallSpace + 2,
    borderLeftColor: colors.danger,
    paddingLeft: metrics.smallSpace
  },

  leftBarDescription: {
    backgroundColor: colors.regular,
    width: metrics.smallSpace,
    marginRight: metrics.smallSpace
  },

  title: {
    fontSize: 14,
    fontWeight: "bold"
  },

  subTitle: {
    fontSize: 12
  },

  user: {
    marginTop: metrics.baseMargin,
    padding: metrics.smallSpace,
    fontSize: metrics.subTitle
  },

  detail: {
    backgroundColor: colors.white,
    margin: metrics.smallSpace,
    fontSize: metrics.subTitle
  },

  jobContainer: {
    marginBottom: metrics.basePadding,
    borderWidth: 1,
    borderRadius: metrics.borderRadius,
    borderColor: colors.dark,
    backgroundColor: colors.white
  },

  jobContainerWithPadding: {
    paddingTop: metrics.basePadding,
  },

  underline: {
    textDecorationLine: "underline"
  },

  address: {
    paddingLeft: metrics.baseMargin
  },

  addressContainer: {
    backgroundColor: colors.lighter
  },

  blockTitle: {
    alignSelf: "center",
    margin: 10,
    marginTop: -5
  },

  button: {
    backgroundColor: colors.primary,
    alignSelf: "stretch",
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    marginTop: metrics.baseMargin
  },
  buttonText: {
    fontWeight: "bold",
    color: colors.white,
    fontSize: 14
  },
  calendarContainer: {
    alignSelf: "center",
    marginBottom: metrics.basePadding
  },

  avatarContainer: {
    borderColor: colors.dark,
    borderWidth: 2 / PixelRatio.get(),
    justifyContent: "center",
    alignItems: "center"
  },
  avatar: {
    // borderRadius: 75,
    width: 135,
    height: 135,
    margin: metrics.baseMargin
  },

  signature: {
    flex: 1,
    borderColor: "#000033",
    borderWidth: 1,
    height: 250
  },

  signatureText:{
    fontSize: 24,
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center'
  },

  buttonStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: 38,
    backgroundColor: "#eeeeee",
    margin: 10
  },

  flexCenter: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  }
});

export default styles;
