import React from 'react';

import { StackNavigator } from 'react-navigation';
import { metrics } from 'styles';

import HeaderRight from 'components/HeaderRight';

// Pages
import Login from 'pages/Login';
import HomeJobs from 'pages/HomeJobs';
import JobsCheckin from 'pages/JobsCheckin';
import DetailJob from 'pages/DetailJob';
import FinishJob from 'pages/FinishJob';
import SignatureJob from 'pages/SignatureJob';
import CloseJob from 'pages/CloseJob';

const createNavigator = (isLogged = false) =>
  StackNavigator(
    {
      Login: { screen: Login },
      HomeJobs: { screen: HomeJobs },
      JobsCheckin: { screen: JobsCheckin },
      DetailJob: { screen: DetailJob },
      FinishJob: { screen: FinishJob },
      SignatureJob: { screen: SignatureJob },
      CloseJob: { screen: CloseJob },

    },
    {
      initialRouteName: isLogged ? "HomeJobs" : "Login",
      navigationOptions: ({ navigation }) => ({
        headerRight: <HeaderRight navigation={navigation} />
      })
    }
  );

export default createNavigator;
