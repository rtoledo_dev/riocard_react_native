/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, AsyncStorage, ActivityIndicator, FlatList } from 'react-native';

import JobBox from 'components/JobBox';

import Icon from 'react-native-vector-icons/FontAwesome';

import { colors } from 'styles';
import styles from './styles';

export default class JobsToBeSync extends Component {
  static navigationOptions = {
    title: 'Serviços a Sincronizar',
    headerTitleStyle: {
      textAlign: 'center',
      alignSelf: 'center',
      flex: 1,
      color: colors.primary
    },
    tabBarIcon: () => <Icon name='hourglass-half' size={20} color={colors.lighter} />
  };

  state = {
    data: [],
    loading: false,
  }

  componentWillMount = () => {
    this.loadingJobs();
  };


  loadingJobs = async () => {
    const auth_token = await AsyncStorage.getItem('@ClientKey:auth_token');
    // const response = await api.post('/jobs',{ auth_token });
    this.setState({data: [{id: 1}, {id: 2}, {id: 3}, {id: 4}]})
  }

  renderFlatListItem = (item) => (
    <JobBox style={styles.height} jobItem={item}  />
  )

  renderList = () => (
    <FlatList data={this.state.data} keyExtractor={item => String(item.id)} renderItem={({item}) => this.renderFlatListItem(item)} />
  );

  render() {
    return (
      <View style={styles.container}>
        { this.state.loading ? <ActivityIndicator style={styles.loading} /> : this.renderList() }
      </View>
    );
  }
}
