import { StyleSheet } from "react-native";
import { metrics } from "styles";

const styles = StyleSheet.create({
  container: {
    padding: metrics.basePadding,
  },

  loading: {
    marginTop: metrics.basePadding,
  },
});

export default styles;
