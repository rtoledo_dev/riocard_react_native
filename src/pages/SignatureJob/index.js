/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { View, ActivityIndicator, Text, TouchableHighlight } from "react-native";
import SignatureCapture from 'react-native-signature-capture';

import SignatureJobBox from "components/SignatureJobBox";

import Icon from "react-native-vector-icons/FontAwesome";

import { colors } from "styles";
import styles from "./styles";

export default class SignatureJob extends Component {
  static navigationOptions = {
    title: "Assinatura da OS",
    headerTitleStyle: {
      textAlign: "center",
      alignSelf: "center",
      flex: 1,
      color: colors.primary
    },
    tabBarIcon: () => (
      <Icon name="hourglass-half" size={20} color={colors.lighter} />
    )
  };

  state = {
    loading: false
  };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: "column" }}>
                <Text style={{alignItems:"center",justifyContent:"center"}}>Signature Capture Extended </Text>
                <SignatureCapture
                    style={[{flex:1},styles.signature]}
                    ref="sign"
                    onSaveEvent={this._onSaveEvent}
                    onDragEvent={this._onDragEvent}
                    saveImageFileInExtStorage={false}
                    showNativeButtons={false}
                    showTitleLabel={false}
                    viewMode={"portrait"}/>

                <View style={{ flex: 1, flexDirection: "row" }}>
                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.saveSign() } } >
                        <Text>Salvar</Text>
                    </TouchableHighlight>

                    <TouchableHighlight style={styles.buttonStyle}
                        onPress={() => { this.resetSign() } } >
                        <Text>Apagar</Text>
                    </TouchableHighlight>

                </View>

            </View>
    );
  }
}
