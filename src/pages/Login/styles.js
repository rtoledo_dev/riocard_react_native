import { StyleSheet } from 'react-native';
import { colors, metrics } from 'styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: metrics.basePadding * 2,
    backgroundColor: colors.white,
    justifyContent: "center",
    alignItems: "stretch"
  },

  title: {
    textAlign: "center",
    color: colors.primary,
    fontSize: 24,
    fontWeight: "bold"
  },

  description: {
    textAlign: "center",
    // marginTop: metrics.baseMargin,
    fontSize: 14,
    color: colors.primary,
    lineHeight: 21,
    fontWeight: "bold"
  },

  error: {
    textAlign: "center",
    color: colors.danger,
    fontWeight: "bold",
    marginTop: metrics.baseMargin,
  },

  form: {
    marginTop: metrics.baseMargin * 2
  },

  input: {
    backgroundColor: colors.white,
    borderColor: colors.primary,
    borderWidth: 1,
    borderRadius: metrics.baseRadius,
    height: 44,
    paddingHorizontal: metrics.basePadding
  },

  button: {
    backgroundColor: colors.primary,
    borderRadius: metrics.baseRadius,
    alignSelf: "stretch",
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    marginTop: metrics.baseMargin
  },

  buttonText: {
    fontWeight: "bold",
    color: colors.white,
    fontSize: 14
  },

  marginTop: {
    marginTop: metrics.baseMargin
  },

  logo: {
    alignSelf: "center"
  }
});

export default styles;

